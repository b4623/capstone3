import {Row, Col , Button} from 'react-bootstrap';
import Image from '../images/background.jpg'


export default function Banner(){
	
	return(
			<Row>
				<Col>
					<h1 className=" text-center p-3s">MasAhIsT@</h1>
					<h5 className=" text-center p-3s">You Deserve to Relax</h5>			
				</Col>
			</Row>
		)
}