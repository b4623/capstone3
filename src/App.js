import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import ECNavbar from './components/ECNavbar';
import Home from './pages/Home';
import WeOffers from './pages/WeOffers';
import ErrorPage from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './userContext';
import WeOffersView from './pages/WeOffersView';
import AdminView from './pages/Admin.js';

function App() {


const Image = new URL("./images/background.jpg", import.meta.url)
//React Context is nothing but a global state to the app. It is a way to make particular data available to all the components no matter how they are nested.

  //State hook for the user state that defined here for a global scope
  //Initialized as an object with properties from the local storage
  //This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  //Function for clearing the localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=> {

      fetch("http://localhost:8000/api/users/details", {
        headers: {
          Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {

        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })
  
  }, [])



  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <ECNavbar />
        <Container className= "main-container">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/weoffers" element={<WeOffers />} />
            <Route path="/weoffers/:weoffersId" element={<WeOffersView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/adminview" element={<AdminView />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>     
      </Router>
    </UserProvider>  
  );
}

export default App;
