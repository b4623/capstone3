
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function WeOffersCard({weoffersProp}) {

	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter, setter] = useState(initalGetterValue)
	*/
	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(5)

	function book() {
	setCount(count + 1)
	setSeats(seats - 1)
	console.log('booked: ' + count)

	}

	useEffect(() => {
	if(seats === 0) {
	alert("No more slot available!");
	}

	}, [seats]);*/


	//console.log(props.courseProp)
	const {name, description, price, _id} = weoffersProp

	return(

			<Card className="m-3 p-6">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Button variant="warning" as={Link} to={`/weoffers/${_id}`} >Details</Button>
				</Card.Body>
			</Card>
		)

}
