import { useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';



export default function WeOffersView() {

	

	const navigate = useNavigate();

	//The useParams hook allows us to retrieve the courseId passed via the URL
	const { weoffersId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const Book = (weoffersId) => {

		fetch("http://localhost:8000/api/user", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				weoffersId: weoffersId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //true //false

			if (data === true) {

				Swal.fire({
					title: "Successfully Booked",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				navigate("/weoffers")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}


		})
	}




	useEffect(() => {
		console.log(weoffersId)
		fetch(`http://localhost:8000/api/products/${weoffersId}`,)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})


	}, [weoffersId])

	return(
			<Container className="mt-5">
				<Row>
					<Col lg={{span:4, offset:3}}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{price}</Card.Text>
								<Card.Subtitle>Class Schedule</Card.Subtitle>
								<Card.Text><Form>
								</Form></Card.Text>
								<Button variant="warning">Book Now</Button>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)


}
