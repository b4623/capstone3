import {Row , Col, Card, Button} from 'react-bootstrap';


export default function Highlights(){
	return (
		<div>
			<Row>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Convenient</Card.Title>
						<Card.Text>No Need to Wait , Book and Arrive on Your Preferred Time. Enjoy and Relax</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Variety</Card.Title>
						<Card.Text>You have plenty of choices what kind of massage will fits you.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Home Service</Card.Title>
						<Card.Text>Enjoy a Home Service Massage in the comfort of your home.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			</Row>

			<Button variant= "warning m-3" className=" text-center">Book Now</Button>

	</div>





		)
}