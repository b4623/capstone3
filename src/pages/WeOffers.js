import {Fragment, useEffect, useState} from 'react';
import WeOfferCard from '../components/WeOfferCards';

export default function WeOffers() {

	const [weoffers, setweoffers] = useState([])
	//console.log(coursesData[0])

	// const courses = coursesData.map(course => {

	// 	return(
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })


	useEffect(() => {
		fetch("http://localhost:8000/api/products")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setweoffers(data.map(weoffers => {
				return(
					<WeOfferCard key={weoffers._id} weoffersProp={weoffers} />
					)
			}))
		})	
	}, [])

	return(
		<Fragment>
			<h1>We Offers</h1>
			{weoffers}
		</Fragment>

		)
}
